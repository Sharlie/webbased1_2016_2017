<%-- 
    Document   : hello
    Created on : Nov 24, 2016, 11:06:29 AM
    Author     : Michiel Noback [m.a.noback@pl.hanze.nl]
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        reads from Attribute map
        <h2>Current time is ${requestScope.date}</h2>
        reads from Parameter map
        <h2>Hi there, ${param.name}</h2>
        <h2>Message= ${requestScope.message}</h2>
        
    </body>
</html>
