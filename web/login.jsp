<%-- 
    Document   : login
    Created on : Nov 28, 2016, 12:58:40 PM
    Author     : Michiel Noback [m.a.noback@pl.hanze.nl]
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Page</title>
    </head>
    <body>
        ${requestScope.errorMessage}
        <h1>Please log in now</h1>
        
        <form action="login.do" method="POST">
            User name: <input type="text" name="username" />
            User password: <input type="password" name="password" />
            <input type="submit" value="OK" />
        </form>
    </body>
</html>
