<%-- 
    Document   : myContentPage
    Created on : Nov 28, 2016, 1:22:15 PM
    Author     : Michiel Noback [m.a.noback@pl.hanze.nl]
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Content Page</title>
    </head>
    <body>
        
        <c:choose>
            <c:when test="${sessionScope.userName != null}">
                <h1>Hi, ${sessionScope.userName} since you are here, you must 
                    be logged in...are you?</h1>
            </c:when>
            <c:otherwise>
                
            </c:otherwise>
            
        </c:choose>
        
    </body>
</html>
