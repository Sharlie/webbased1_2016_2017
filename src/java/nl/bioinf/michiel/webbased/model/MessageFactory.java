/*
 * Copyright (c) 2016 Michiel Noback [m.a.noback@pl.hanze.nl]
 * All rights reserved.
 */
package nl.bioinf.michiel.webbased.model;

import java.text.DateFormatSymbols;
import java.util.Calendar;

/**
 *
 * @author Michiel Noback [m.a.noback@pl.hanze.nl]
 * @version 0.0.1
 */
public class MessageFactory {

    public static String getMessage() {
        String[] dayNames = new DateFormatSymbols().getWeekdays();
        Calendar calendar = Calendar.getInstance();
        StringBuilder sb = new StringBuilder();
        sb.append("Today is a ");
        sb.append(dayNames[calendar.get(Calendar.DAY_OF_WEEK)]);
        sb.append("; ");
        
        return sb.toString();
    }

}
