/*
 * Copyright (c) 2016 Michiel Noback [m.a.noback@pl.hanze.nl]
 * All rights reserved.
 */
package nl.bioinf.michiel.webbased.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Michiel Noback [m.a.noback@pl.hanze.nl]
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/login.do"})
public class LoginServlet extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher view = request.getRequestDispatcher("login.jsp");
        view.forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        response.getWriter().println("Login request received");

        String userName = request.getParameter("username");
        String password = request.getParameter("password");

        System.out.println("password = " + password);
        System.out.println("userName = " + userName);

        String viewJsp = "myContentPage.jsp";
        HttpSession session = request.getSession();
        
        //TODO remove at deployment!
        session.setMaxInactiveInterval(5);
        
        if (session.isNew() || session.getAttribute("userName") == null) {
            if (userName.equals("Henk") && password.equals("Henk")) {
                System.out.println("Logged IN!");
                session.setAttribute("userName", userName);
            }
            else {
                session.invalidate();
                request.setAttribute("errorMessage", "Sorry, your credentials are not OK. Please try again");
                viewJsp = "login.jsp";
            }
        }
        RequestDispatcher view = request.getRequestDispatcher(viewJsp);
        view.forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
